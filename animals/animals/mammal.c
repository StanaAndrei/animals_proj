#include "mammal.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>

void foodMenuUI(void) {
  puts("1.carnivore");
  puts("2.herbivore");
  puts("3.omnivore");
  printf("choose: ");
}

bool readMammal(Mammal *mammal) {
  int r = 0;
  printf("weight: ");
  r += scanf("%d", &mammal->weight);
  printf("height: ");
  r += scanf("%d", &mammal->height);
  foodMenuUI();
  r += scanf("%d", (int*)&mammal->ftp);
  return r == 3;
}

void printMammal(const Mammal *const mammal) {
  const char format[] = "{ wight: %d, height: %d, food-type: %s }\n";
  char ftps[10];
  switch (mammal->ftp) {
  case CARNIVORE:
    strcpy(ftps, "carnivore");
    break;
  case HERBIVORE:
    strcpy(ftps, "herbivore");
    break;
  case OMNIVORE:
    strcpy(ftps, "omnivore");
    break;
  default:
    assert(false);
  }
  printf(format, mammal->weight, mammal->height, ftps);
}
