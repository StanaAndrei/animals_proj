#ifndef __INSECT_H
#define __INSECT_H
#include <stdbool.h>

typedef struct {
  int legs, lifeSpan;
} Insect;

bool readInsect(Insect*);
void printInsect(const Insect *const);

#endif
