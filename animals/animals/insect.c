#include "insect.h"
#include <stdbool.h>
#include <stdio.h>

bool readInsect(Insect *insect) {
  int r = 0;
  printf("legs: ");
  r += scanf("%d", &insect->legs);
  printf("lise span: ");
  r += scanf("%d", &insect->lifeSpan);
  return r == 2;
}

void printInsect(const Insect *const insect) {
  const char format[] = "{ legs: %d, life-span: %d }\n";
  printf(format, insect->legs, insect->lifeSpan);
}
