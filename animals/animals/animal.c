#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include "animal.h"
#include "bird.h"
#include "insect.h"
#include "mammal.h"

bool readAnimal(Animal *animal) {
  bool ok = false;
  puts("1.bird");
  puts("2.insect");
  puts("3.mammal");
  printf("type: ");
  {
    int tp;
    if (scanf("%d", &tp) != 1) {
      return 0;
    }
    tp--;
    animal->tp = (AnimalType)tp;
  }
  
  switch (animal->tp) {
  case BIRD:
    ok = readBird(&animal->birdData);
    break;
  case INSECT:
    ok = readInsect(&animal->insectData);
    break;
  case MAMMAL:
    ok = readMammal(&animal->mammalData);
    break;
  default:
    fprintf(stderr, "wrong animal!\n");
  }
  return ok;
}

void printAnimal(const Animal *const animal) {
  switch(animal->tp) {
  case BIRD:
    printBird(&animal->birdData);
    break;
  case INSECT:
    printInsect(&animal->insectData);
    break;
  case MAMMAL:
    printMammal(&animal->mammalData);
    break;
  default:
    assert(false);
  }
}
