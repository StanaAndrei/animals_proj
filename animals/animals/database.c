#include "animal.h"
#include <stdlib.h>
#include <stdio.h>
#include "database.h"

Database *checkAlloc(Database **db) {
  if ((*db)->at == NULL) {
    free(*db);
    *db = NULL;
  }
  return *db;
}

void addToDb(Database *db, Animal animal) {
  if (db != NULL && db->len == db->mem) {
    db->mem += CHUNK;
    db->at = realloc(db->at, sizeof(Animal) * db->mem);
    db = checkAlloc(&db);
  }
  if (db != NULL) {
    db->at[(db->len)++] = animal;
  }
}

Database *mkDb(void) {
  Database *db = malloc(sizeof(Database));
  db->len = db->mem = 0;
  db->at = NULL;
  return db;
}

void delAt(Database *db, int idx) {
  for (int i = idx + 1; i < db->len; i++) {
    db->at[i - 1] = db->at[i];
  }
  db->len--;
}

void printAll(const Database *const db) {
  for (int i = 0; i < db->len; i++) {
    printf("%d: ", i + 1);
    printAnimal(&db->at[i]);
  }
}
