#ifndef __MAMMAL_H
#define __MAMMAL_H
#include <stdbool.h>

typedef enum {
  CARNIVORE = 1, HERBIVORE, OMNIVORE
} FoodType;

typedef struct {
  int weight, height;
  FoodType ftp;
} Mammal;

bool readMammal(Mammal*);
void printMammal(const Mammal *const);

#endif
