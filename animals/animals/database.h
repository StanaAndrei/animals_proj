#ifndef __DATABASE_H
#define __DATABASE_H
#include "animal.h"
#define CHUNK (1 << 8)

typedef struct {
  int len, mem;
  Animal *at;
} Database;

void addToDb(Database*, Animal);
Database *mkDb(void);
void delAt(Database*, int);
void printAll(const Database *const);

#endif
