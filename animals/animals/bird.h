#ifndef __BIRD_H
#define __BIRD_H

typedef struct {
  int speed, wingsLen;
} Bird;

int readBird(Bird*);
void printBird(const Bird *const);

#endif
