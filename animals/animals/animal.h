#ifndef __ANIMAL_H
#define __ANIMAL_H
#include <stdbool.h>
#include "bird.h"
#include "insect.h"
#include "mammal.h"

typedef enum {
  BIRD, INSECT, MAMMAL
} AnimalType;

typedef struct {
  AnimalType tp;
  union {
    Bird birdData;
    Insect insectData;
    Mammal mammalData;
  };
} Animal;

bool readAnimal(Animal*);
void printAnimal(const Animal *const);

#endif
