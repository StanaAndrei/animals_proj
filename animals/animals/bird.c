#include <stdio.h>
#include "bird.h"

int readBird(Bird *bird) {
  int r = 0;
  printf("speed: ");
  r += scanf("%d", &(bird->speed));
  printf("wings-length: ");
  r += scanf("%d", &(bird->wingsLen));
  return r == 2;
}

void printBird(const Bird *const bird) {
  const char format[] = "{ speed: %d, wings-length: %d }\n";
  printf(format, bird->speed, bird->wingsLen);
}
