#include <stdio.h>
#include <stdlib.h>
#include "database.h"
#include "animal.h"
#define EVER ;;

typedef enum {
  ADD = 1, DEL, PRINT, EXIT
} OpsEnum;

void menuUI() {
  puts("");
  puts("1.add");
  puts("2.del");
  puts("3.print");
  puts("4.exit");
  printf("choose: ");
}

int main(void) {
  Database *db = mkDb();
  OpsEnum op;
  for (EVER) {
    menuUI();
    scanf("%d", (int*)&op);
    switch(op) {
    case ADD:
      Animal animal;
      readAnimal(&animal);
      addToDb(db, animal);
      break;
    case DEL:
      int idx;
      printf("at: ");
      scanf("%d", &idx);
      delAt(db, idx - 1);
      break;
    case PRINT:
      printAll(db);
      break;
    case EXIT:
      free(db);
      exit(EXIT_SUCCESS);
    }
  }
  return 0;
}
